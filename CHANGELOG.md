# Changelog

#### v0.1.6

- Chore: Support base 3

#### v0.1.5

- Fix: windows import of system library

#### v0.1.4

- Chore: support newer containers, text and system

#### v0.1.3

- Chore: support base 2

#### v0.1.2

- Fix windows builds (misplaced library files)

#### v0.1.1

- Update platform versions and use non-bootstrap image

### v0.1

- Initial nitrile version

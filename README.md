# Serial

Serial port library for [clean](http://clean.cs.ru.nl).

## Platform notes

### Windows

To cross-compile for windows 64-bit on linux, install `gcc-mingw-w64` and `gcc-multilib` and run:

```
CC=x86_64-w64-mingw32-gcc-win32 CFLAGS=-m64 nitrile build --platform=windows
```

## Maintainer

Mart Lubbers (mart@cs.ru.nl)

## License

`serial` is licensed under the BSD 2-Clause "Simplified" License (see [LICENSE](LICENSE)).
